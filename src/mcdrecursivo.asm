; EIF400 Paradigmas de Programación
; Autores:
; Rubén Pereira Ugalde
; Gabriel Lizano Alvarado
; Luis Guillermo Ramírez Diaz
; Edwin Daniel Lobo Sánchez

; Profesor: Carlos Loría Sáenz

jmp start
var1: DB "gcd = ",0
var2: DB 0
num1: DB 169
num2: DB 13

start:
	mov A, [num1] ; num1
	mov B, [num2] ; num2
	call mcd      ; Hace un llamado hacia el mcd
	mov C, var1
	mov D, 232	  ; Alista el output 
	push C
	call print
	pop C
   	hlt           ; Termina la ejecucion
	
; ----> Algoritmo de mcd (Recursivo de cola)
mcd:
	cmp b,0
	jz esCero
	mov d,a
	div b
	mul b
	sub d,a
	mov a,b
	mov b,d
	call mcd ; llamado recursivo
	jmp return

esCero:
	mov d, b
	ret

return:
	ret
; ----------------------------------------
; ---> Algoritmo para impresion.

print:			 ; print(C:*from, D:*to)
	push A
	mov B, 0
	
.loop:
	mov A, [C]	 ; Obtiene el char de la variable
	mov [D], A	 ; Escribe la salida
	inc C        ; incrementa C, la cual esta apuntando a la variable
	inc D  
	cmp B, [C]	 ; Pregunta si tiene un 0
	jnz .loop	 ; Continua si no
	pop A
	mov [var2],A   ; Después de que realice el pop, A vuelve 					 
                   ; con el resultado del mcd 
	push 255
.looppush:
	mov A, [var2]   
	mov B,A
	div 10
	mov [var2], A
	mul 10
	sub B,A
	push B
	mov B, [var2]
	cmp B,0
	jnz .looppush	
	
.looppop:
	pop A
	cmp A, 255
	je .end
	add A,48
	mov [D],A
	inc D
	jmp .looppop
.end: 	
	ret