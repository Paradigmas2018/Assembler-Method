; EIF400 Paradigmas de Programación
; Autores:
; Rubén Pereira Ugalde
; Gabriel Lizano Alvarado
; Luis Guillermo Ramírez Diaz
; Edwin Daniel Lobo Sánchez

; Profesor: Carlos Loría Sáenz

JMP start
var1: DB "gcd = ",0
var2: DB 0
var3: DB 169
var4: DB 26

start:
	MOV A, [var3]
	MOV B, [var4]
	CALL mcd    ; Hace un llamado hacia el mcd
	MOV C, var1
	MOV D, 232	; Alista el output 
	PUSH C
	CALL print
	POP C
   	HLT         ; Termina la ejecucion
	
mcd:	        ; Determina si alguno de las variables es 0.
	CMP A,0
	JZ returnA
	CMP B, 0 
	JZ returnB
	call mcd1
	RET
mcd1:
   .loop:
	CMP A,B
	JA amayorb
	JB bmayora
	CMP A,B
	JNE .loop    ;Continua en el ciclo si no son iguales.
	RET

returnA:
	MOV A, B
	RET
	
returnB:
	RET
	
amayorb:         ; a = a - b
	SUB A,B
	JMP mcd1
	
bmayora:         ; b = b - a		
	SUB B,A	
	JMP mcd1

print:			 ; print(C:*from, D:*to)
	PUSH A
	MOV B, 0
	
.loop:
	MOV A, [C]	 ; Obtiene el char de la variable
	MOV [D], A	 ; Escribe la salida
	INC C        ; Incrementa C, la cual esta apuntando a la variable
	INC D  
	CMP B, [C]	 ; Pregunta si tiene un 0
	JNZ .loop	 ; Continua si no
	POP A
	MOV [var2],A   ; Después de que realice el POP, A vuelve 					 
                   ; con el resultado del mcd 
	PUSH 255
.looppush:
	MOV A, [var2]   
	MOV B,A
	DIV 10
	MOV [var2], A
	MUL 10
	SUB B,A
	PUSH B
	MOV B, [var2]
	CMP B,0
	JNZ .looppush	
	
.looppop:
	POP A
	CMP A, 255
	JE .end
	ADD A,48
	MOV [D],A
	INC D
	JMP .looppop
.end: 	
	RET
